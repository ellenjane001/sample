<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <title>Document</title>
</head>

<body>
    <div class="main">
        <div class="navbar">
            <!-- <img src="https://img.icons8.com/glyph-neue/256/4a90e2/myspace.png" /> -->
            <a href="">
                <img src="https://img.icons8.com/glyph-neue/64/4a90e2/dashboard.png" />
                <br> Dashboard
            </a>
            <a href="">B</a>
            <a href="">B</a>
            <a href="">B</a>
        </div>
        <div class="container">
            <div class="container-top">
                <div>
                    <!-- <span>DASHBOARD</span> -->
                </div>
                <div class="container-top-user"></div>
            </div>
            <div class="container-center">
                <div class="container-center-item">
                    <div class="container-center-items">
                        <span>Total Tickets</span>
                    </div>
                    <div class="container-center-items">
                        <span>Open</span>
                    </div>
                    <div class="container-center-items">
                        <span>Pending</span>
                    </div>
                    <div class="container-center-items">
                        <span>Resolved</span>
                    </div>
                </div>
                <div class="container-center-item-1"></div>
            </div>
            <div class="container-bottom"></div>
        </div>
    </div>
</body>

</html>